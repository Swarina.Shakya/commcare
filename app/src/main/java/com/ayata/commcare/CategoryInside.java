package com.ayata.commcare;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class CategoryInside extends AppCompatActivity {

 public Button btn_next,btn_prev,_btn_skip;
 public RadioButton rb1,rb2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_inside);

        btn_next=findViewById(R.id.ci_btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(CategoryInside.this, CategoryTraining.class);
                startActivity(intent);
                finish();
            }
        });



        rb1=findViewById(R.id.ci_radio_option1);
        rb2=findViewById(R.id.ci_radio_option2);
        RadioGroup radioGroup=findViewById(R.id.ci_radiogroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == rb1.getId()){
                    Log.d("radio", "onCreate: rb1");
                    rb1.setBackground(getResources().getDrawable(R.drawable.radio_outer_select));
                    rb2.setBackground(getResources().getDrawable(R.drawable.radio_outer_unselect));
                }else{
                    Log.d("radio", "onCreate: rb2");
                    rb1.setBackground(getResources().getDrawable(R.drawable.radio_outer_unselect));
                    rb2.setBackground(getResources().getDrawable(R.drawable.radio_outer_select));
                }
            }
        });

    }

}
