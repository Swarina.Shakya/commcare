package com.ayata.commcare;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class CategoryTraining extends AppCompatActivity {

    private Button btn_prev,btn_next,btn_skip;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_training);

        btn_next=findViewById(R.id.ct_btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(CategoryTraining.this, MainUserProfile.class);
                startActivity(intent);
                finish();
            }
        });

        btn_prev=findViewById(R.id.ct_btn_previous);
        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(CategoryTraining.this, CategoryInside.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
