package com.ayata.commcare;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.hornet.dateconverter.DateConverter;
import com.hornet.dateconverter.DatePicker.DatePickerDialog;
import com.hornet.dateconverter.Model;
import com.hornet.dateconverter.TimePicker.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CreateNewUser extends AppCompatActivity implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, com.hornet.dateconverter.CalendarView.Calendar.OnDateSetListener {

    RadioButton rb_1, rb_2, rb_3;

    private Button btn;
    View toolbar;

    TextInputLayout textInputName;

    private String spinner_text;

    DateConverter dateConverter;
    TextInputLayout cnu_dob;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_user);

        init();

        toolbar = findViewById(R.id.cnu_toolbar);
        TextView textView = toolbar.findViewById(R.id.appbar_text);
        textView.setText("Create New User");

        textInputName = (TextInputLayout) findViewById(R.id.cnu_name);

        btn = findViewById(R.id.cnu_btn_submit);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!validateName() | !validateGender()){
                    Toast.makeText(CreateNewUser.this, "Fill the Necessary Field!",
                            Toast.LENGTH_LONG).show();
                    return;

                }else {
                    Intent i = new Intent(CreateNewUser.this, PatientAddress4.class);
                    startActivity(i);
                    finish();
                }
            }
        });

        //datepicker//////////////////////////////////////////////
        dateConverter = new DateConverter();
        cnu_dob=findViewById(R.id.cnu_dob);

        spinner= findViewById(R.id.cnu_wardno);

        String[] wards={"Ward no. 1","Ward no. 1 Naka","Ward no. 2","Ward no. 3","Ward no. 4","Ward no. 5","Ward no. 6","Ward no. 6 Naka","Ward no. 7","Ward no. 7 Naka"};


        ArrayAdapter<String> adapter= new ArrayAdapter<>(CreateNewUser.this, R.layout.dropdown,R.id.dropdown_text, wards);
        spinner.setAdapter(adapter);
        spinner.setPrompt("Select Ward No.");
        spinner.setSelected(true);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // get text from spinner
                spinner_text= spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }

    public void dateClick(View v){
        Calendar now = Calendar.getInstance();
        DatePickerDialog datePickerDialog =DatePickerDialog.newInstance(this,dateConverter.getNepaliDate(now));
        datePickerDialog.show(getSupportFragmentManager(), "DatePicker");


    }

    public void init() {
        rb_1 = (RadioButton) findViewById(R.id.cnu_radio_female);
        rb_2 = findViewById(R.id.cnu_radio_male);
        rb_3 = findViewById(R.id.cnu_radio_other);
        rb_1.setOnClickListener(this);
        rb_2.setOnClickListener(this);
        rb_3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        boolean checked = ((RadioButton) v).isChecked();
        switch (v.getId()) {
            case R.id.cnu_radio_female:
                if (checked) {
                    rb_2.setChecked(false);
                    rb_3.setChecked(false);
                }
                break;
            case R.id.cnu_radio_male:
                if (checked) {
                    rb_1.setChecked(false);
                    rb_3.setChecked(false);
                }
                break;
            case R.id.cnu_radio_other:
                if (checked) {
                    rb_1.setChecked(false);
                    rb_2.setChecked(false);
                }
                break;
        }
    }

    private Boolean validateName() {
        String name = textInputName.getEditText().getText().toString().trim();

        if (name.isEmpty()) {
            textInputName.setError("Field can't be empty");
            return false;
        }else{
            textInputName.setError(null);
            return true;
        }

    }

    private Boolean validateGender(){

        if(rb_1.isChecked()| rb_2.isChecked()| rb_3.isChecked()){
            return true;
        }
        return false;
    }


    public void OnWardnoClick(View v){
        Intent i = new Intent(CreateNewUser.this, PatientAddress.class);
        startActivity(i);
        finish();
    }


    //datepicker
    @Override
    public void onDateClick(View calendar, int year, int month, int day) {
        Toast.makeText(this, "year :: " + year + "  month :: " + month + " day :: " + day, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        int month=monthOfYear+1;
        String date =dayOfMonth + "/" + month + "/" + year;
        Log.d("tag","set text");
        cnu_dob.getEditText().setText(date);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

    }
}


