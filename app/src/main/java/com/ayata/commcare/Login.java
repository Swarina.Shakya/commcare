package com.ayata.commcare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ScrollView;

import com.google.android.material.textfield.TextInputLayout;

public class Login extends AppCompatActivity {
private Button btn;

private ScrollView scrollView;
private View insideView;

private TextInputLayout phonenolayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //login button
        btn=findViewById(R.id.btn_login);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!validatePhone()){
                    return;
                }
                    Intent intent = new Intent(Login.this, Verification.class);
                    startActivity(intent);
                    finish();
            }
        });

        scrollView = findViewById(R.id.login_scrollview);
        insideView= findViewById(R.id.btn_login);

        phonenolayout= findViewById(R.id.login_phone_no);

        checkforKeyboard(scrollView);



    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    private void closeKeyboard(){
        View view= this.getCurrentFocus();
        if(view != null){
            InputMethodManager inputMethodManager= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }

    public void scrolltoRow(){
//        scrollView.scrollTo(0, (int)insideView.getY());

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    public void checkforKeyboard(final View activityRootView){

        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > dpToPx(Login.this, 200)) { // if more than 200 dp, it's probably a keyboard...
                    // ... do something here
                    scrolltoRow();
                }
            }
        });

    }

    private Boolean validatePhone(){
        String phone_number = phonenolayout.getEditText().getText().toString().trim();

        if (phone_number.isEmpty()) {
            phonenolayout.setError("Field can't be empty");
            return false;
        }else{
            phonenolayout.setError(null);
            closeKeyboard();
            return true;
        }
    }
}
