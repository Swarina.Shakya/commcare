package com.ayata.commcare;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {
    private Button btn;
    private static int TIME_OUT = 800; //Time to launch the another activity

    Handler handler= new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        ImageView imageView= findViewById(R.id.main_image);
//        Glide.with(MainActivity.this).load(R.drawable.addimage).into(imageView);

        btn=findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(MainActivity.this, GetStarted.class);
                startActivity(intent);
                MainActivity.this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                finish();
            }
        });


//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Intent i = new Intent(MainActivity.this, GetStarted.class);
//                startActivity(i);
//                MainActivity.this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
//                finish();
//            }
//        }, TIME_OUT);
    }



}
