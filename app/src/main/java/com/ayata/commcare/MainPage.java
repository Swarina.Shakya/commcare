package com.ayata.commcare;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.ayata.commcare.fragment.FragmentCharts;
import com.ayata.commcare.fragment.FragmentHome;
import com.ayata.commcare.fragment.FragmentProfile;
import com.ayata.commcare.fragment.FragmentUserList;
import com.ayata.commcare.fragment.FragmentVideoArticles;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainPage extends FragmentActivity {

    public static FragmentManager fragmentManager;

    ImageView add, sync;
    View toolbar;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);

        fragmentManager= getSupportFragmentManager();
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();

       getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_page,new FragmentHome()).commit();

        BottomNavigationView bottomNavigationView= findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        toolbar= findViewById(R.id.main_page_toolbar);

        add= toolbar.findViewById(R.id.toolbar_add);
        sync= toolbar.findViewById(R.id.toolbar_sync);

        add.setVisibility(View.GONE);
        sync.setVisibility(View.VISIBLE);


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener=
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment= null;

                    switch (menuItem.getItemId()){

                        case R.id.nav_home:
                            selectedFragment = new FragmentHome();
                            add.setVisibility(View.GONE);
                            sync.setVisibility(View.VISIBLE);
                            break;

                        case R.id.nav_gallary:
                            selectedFragment = new FragmentVideoArticles();
                            add.setVisibility(View.GONE);
                            sync.setVisibility(View.VISIBLE);
                            break;

                        case R.id.nav_add:
                            selectedFragment = new FragmentUserList();
                            sync.setVisibility(View.GONE);
                            add.setVisibility(View.VISIBLE);

                            add.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent= new Intent(MainPage.this, CreateNewUser.class);
                                    startActivity(intent);
                                    //finishAffinity();
                                }
                            });
                            break;

                        case R.id.nav_analytics:
                            selectedFragment = new FragmentCharts();
                            sync.setVisibility(View.GONE);
                            add.setVisibility(View.GONE);
                            break;

                        case R.id.nav_more:
                            selectedFragment = new FragmentProfile();
                            sync.setVisibility(View.GONE);
                            add.setVisibility(View.GONE);
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_page,selectedFragment).addToBackStack(null).commit();
                    return true;
                }
            };
}
