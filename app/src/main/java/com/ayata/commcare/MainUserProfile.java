package com.ayata.commcare;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.ayata.commcare.fragment.FragmentCharts;
import com.ayata.commcare.fragment.FragmentHome;
import com.ayata.commcare.fragment.FragmentProfile;
import com.ayata.commcare.fragment.FragmentUserProfile;
import com.ayata.commcare.fragment.FragmentVideoArticles;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainUserProfile extends FragmentActivity {
    public static FragmentManager fragmentManager;


    View toolbar;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_user_profile);

        fragmentManager= getSupportFragmentManager();
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();

        if(findViewById(R.id.fragment_main_user_profile)!=null){
            if(savedInstanceState!=null){
                return;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_user_profile,new FragmentUserProfile()).commit();
        }



//
//        BottomNavigationView bottomNavigationView= findViewById(R.id.main_user_profile_bottom_navigation);
//        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        toolbar= findViewById(R.id.main_user_profile_toolbar);
        TextView textView = toolbar.findViewById(R.id.appbar_text);
        textView.setText("User Profile");



    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener=
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment= null;

                    switch (menuItem.getItemId()){

                        case R.id.nav_home:
                            selectedFragment = new FragmentHome();
                            break;

                        case R.id.nav_gallary:
                            selectedFragment = new FragmentVideoArticles();

                            break;

                        case R.id.nav_add:
                            selectedFragment = new FragmentUserProfile();

                            break;

                        case R.id.nav_analytics:
                            selectedFragment = new FragmentCharts();

                            break;

                        case R.id.nav_more:
                            selectedFragment = new FragmentProfile();

                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_user_profile,selectedFragment).addToBackStack(null).commit();
                    return true;
                }
            };



}




