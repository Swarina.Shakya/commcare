package com.ayata.commcare;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.commcare.adapter.AdapterAddress;
import com.ayata.commcare.model.ModelAddress;

import java.util.ArrayList;
import java.util.List;

public class PatientAddress extends AppCompatActivity implements AdapterAddress.SingleClickListener{

    RecyclerView recyclerView;
    AdapterAddress adapter;
    int layout;
    List<ModelAddress> list;
    ModelAddress modelAddress;

    private Button btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_address);

        btn= findViewById(R.id.pa_btn_ok);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(PatientAddress.this,CreateNewUser.class);
                startActivity(intent);
                finish();
            }
        });

        recyclerView=findViewById(R.id.recycler_patientaddress);
        list=new ArrayList<>();
        DataPrepare();

        //layout=R.layout.activity_layout_designation_single;

        adapter=new AdapterAddress(list,R.layout.activity_layout_designation_single);

        adapter.setOnItemClickListener(this);
        RecyclerView.LayoutManager manager= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);


    }



    void DataPrepare(){
        list.add(new ModelAddress("Ward no. 1"));
        list.add(new ModelAddress("Ward no. 1 Naka"));
        list.add(new ModelAddress("Ward no. 2"));
        list.add(new ModelAddress("Ward no. 3"));
        list.add(new ModelAddress("Ward no. 4"));
        list.add(new ModelAddress("Ward no. 5"));
        list.add(new ModelAddress("Ward no. 6"));
        list.add(new ModelAddress("Ward no. 6 Naka"));
        list.add(new ModelAddress("Ward no. 7"));
        list.add(new ModelAddress("Ward no. 7 Naka"));


    }
    @Override
    public void onItemClickListener(int position) {
        adapter.selectedItem(position);
    }
}



