package com.ayata.commcare;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class PatientAddress4 extends FragmentActivity implements OnMapReadyCallback {

    GoogleMap map;
    private Button btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_address_4);
        SupportMapFragment mapFragment=(SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        View headerLayout = findViewById(R.id.pa4_toolbar);
        TextView headerText=(TextView)headerLayout.findViewById(R.id.tc_title);
        headerText.setText("Address");


        btn= findViewById(R.id.pa4_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(PatientAddress4.this, MainUserProfile.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map=googleMap;
        LatLng home=new LatLng(27.708480, 85.305539);
        map.addMarker(new MarkerOptions().position(home).title("home"));
        map.moveCamera(CameraUpdateFactory.newLatLng(home));

    }
}
