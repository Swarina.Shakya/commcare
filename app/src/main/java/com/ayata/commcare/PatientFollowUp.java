package com.ayata.commcare;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.commcare.adapter.AdapterPatientFollowUp;
import com.ayata.commcare.model.ModelPatientFollowuP;

import java.util.ArrayList;
import java.util.List;

public class PatientFollowUp extends AppCompatActivity implements AdapterPatientFollowUp.OnPatientFollowUpClickListener {

    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    AdapterPatientFollowUp adapterPatientFollowUp;
    List<ModelPatientFollowuP> listitem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_follow_up);

        View toolbar= findViewById(R.id.pfu_toolbar);
        TextView header= toolbar.findViewById(R.id.appbar_text);
        header.setText("Follow Ups");


        ImageView back= toolbar.findViewById(R.id.appbar_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(PatientFollowUp.this,MainPage.class);
                startActivity(intent);
                finish();
            }
        });

        recyclerView= findViewById(R.id.recycler_patientfollowup);
        listitem= new ArrayList<>();
        adapterPatientFollowUp= new AdapterPatientFollowUp(this,listitem,this);
        linearLayoutManager= new LinearLayoutManager(this);
        recyclerView.setAdapter(adapterPatientFollowUp);
        recyclerView.setLayoutManager(linearLayoutManager);
        dataPrepare();
    }

    private void dataPrepare(){

        listitem.add(new ModelPatientFollowuP("12/01/2020","21/01/2020","Maternal Care",R.drawable.person4));
        listitem.add(new ModelPatientFollowuP("12/01/2020","21/01/2020","Maternal Care",R.drawable.person4));
        listitem.add(new ModelPatientFollowuP("12/01/2020","21/01/2020","Maternal Care",R.drawable.person4));
        listitem.add(new ModelPatientFollowuP("12/01/2020","21/01/2020","Maternal Care",R.drawable.person4));
    }

    @Override
    public void onPatientFollowUpClick(int position) {

        Intent intent= new Intent(this,MainUserProfile.class);
        startActivity(intent);
        finishAffinity();
    }
}
