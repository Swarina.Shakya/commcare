package com.ayata.commcare;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.hornet.dateconverter.DateConverter;
import com.hornet.dateconverter.DatePicker.DatePickerDialog;
import com.hornet.dateconverter.Model;
import com.hornet.dateconverter.TimePicker.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Register_CHW extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, com.hornet.dateconverter.CalendarView.Calendar.OnDateSetListener{

    private Button btn;
    private Spinner spinner;

    DateConverter dateConverter;
    EditText cnu_dob_edittext;
    TextInputLayout dob;

    private String spinner_text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_chw);

        btn=findViewById(R.id.register_chw_btn_submit);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Register_CHW.this, CheckList.class);
                startActivity(intent);
                finishAffinity();
            }
        });

//        String designation = getIntent().getStringExtra("designation");
//
//        TextView textView= findViewById(R.id.register_chw_designation);
//        textView.setText(designation);

        //datepicker//////////////////////////////////////////////
        dateConverter = new DateConverter();

        dob= findViewById(R.id.register_chw_dob);

        spinner= findViewById(R.id.register_chw_designation);

        String[] designations={"CHW 1","CHW 1 Naka","CHW 2","CHW 3","CHW 4","CHW 5","CHW 6","CHW 6 Naka","CHW 7","CHW 7 Naka"};


        ArrayAdapter<String> adapter= new ArrayAdapter<>(Register_CHW.this, R.layout.dropdown,R.id.dropdown_text, designations);
        spinner.setAdapter(adapter);
        spinner.setPrompt("Select Designation");
        spinner.setSelected(true);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // get text from spinner
                spinner_text= spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


    }


    public void OnSelectDesignation(View v){
        Intent intent = new Intent(Register_CHW.this, SelectDesignation.class);
        startActivity(intent);
    }

    public void dateClick(View v){
        Calendar now = Calendar.getInstance();
        DatePickerDialog datePickerDialog =DatePickerDialog.newInstance(this,dateConverter.getNepaliDate(now));
        datePickerDialog.show(getSupportFragmentManager(), "DatePicker");


    }

    //datepicker
    @Override
    public void onDateClick(View calendar, int year, int month, int day) {
        Toast.makeText(this, "year :: " + year + "  month :: " + month + " day :: " + day, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        int month=monthOfYear+1;
        //String date = "You picked the following date: " + dayOfMonth + " " + getResources().getString(DateConverter.getNepaliMonthString(monthOfYear)) + " " + year;
        String date =dayOfMonth + "/" + month + "/" + year;
        Log.d("tag","set text");
        dob.getEditText().setText(date);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

    }
}
