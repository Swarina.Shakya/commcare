package com.ayata.commcare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.ayata.commcare.adapter.AdapterSelectDesignation;
import com.ayata.commcare.model.ModelDesignation;

import java.util.ArrayList;
import java.util.List;

public class SelectDesignation extends AppCompatActivity implements AdapterSelectDesignation.SingleClickListener{

    private List<ModelDesignation> list= new ArrayList<>();
    private  AdapterSelectDesignation adapter;
    private Button btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_designation);
        RecyclerView recyclerView=findViewById(R.id.recyclerid);

        btn= findViewById(R.id.select_destination_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(SelectDesignation.this, Register_CHW.class);
                intent.putExtra("designation","CHW 1 NAKA");
                startActivity(intent);

            }
        });
//       adapter = new AdapterSelectDesignation(list,this);
        adapter = new AdapterSelectDesignation(list);

        RecyclerView.LayoutManager manager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        DataPrepare();
    }

    public  void  DataPrepare(){
        list.add(new ModelDesignation("CHW 1"));
        list.add(new ModelDesignation("CHW 1 Naka"));
        list.add(new ModelDesignation("CHW 2"));
        list.add(new ModelDesignation("CHW 3"));
        list.add(new ModelDesignation("CHW 4"));
        list.add(new ModelDesignation("CHW 5"));
        list.add(new ModelDesignation("CHW 6"));
        list.add(new ModelDesignation("CHW 6 Naka"));
        list.add(new ModelDesignation("CHW 7"));
        list.add(new ModelDesignation("CHW 7 Naka"));
    }

    @Override
    public void onItemClickListener(int position) {
//        Drawable drawable=getResources().getDrawable(R.drawable.submit_button);
        adapter.selectedItem(position);
        Log.d("helo","selected item before" +position);


    }
}
