package com.ayata.commcare;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SinglePost extends AppCompatActivity {

    private View toolbar;
    private ImageView back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_post);

        toolbar= findViewById(R.id.single_post_toolbar);

        back= (ImageView)toolbar.findViewById(R.id.toolbar_back_backbtn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(SinglePost.this, MainPage.class);
                startActivity(intent);
                SinglePost.this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                finishAffinity();
            }
        });
    }
}
