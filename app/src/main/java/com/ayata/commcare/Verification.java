package com.ayata.commcare;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.goodiebag.pinview.Pinview;

public class Verification extends AppCompatActivity {
    private Button btn;
    private ScrollView scrollView;
    private View insideView;
    private Pinview pinview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification);

        scrollView= findViewById(R.id.verification_scrollview);
        insideView = findViewById(R.id.verification_btn);
                // find the View that you need to scroll to which is inside this ScrollView

        pinview= findViewById(R.id.verification_pin);

        View activityRootView = findViewById(R.id.verification_root);

        checkforKeyboard(activityRootView);

//        CheckForKeyboard checkForKeyboard = new CheckForKeyboard();
//
//       Boolean keyboard= (Boolean)checkForKeyboard.IsKeyboardPresent(activityRootView,Verification.this);
//
//       if(keyboard== true){
//           scrolltoRow();
//       }


        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {


            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                //Make api calls here or what not
                closeKeyboard();
                Toast.makeText(Verification.this, pinview.getValue(), Toast.LENGTH_SHORT).show();
            }


        });

        btn=findViewById(R.id.verification_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                closeKeyboard();
                Intent intent= new Intent(Verification.this, Register_CHW.class);
                startActivity(intent);
                finish();

            }
        });
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    private void closeKeyboard(){
        View view= this.getCurrentFocus();
        if(view != null){
            InputMethodManager inputMethodManager= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }

    public void scrolltoRow(){
        //scrollView.scrollTo(0, (int)insideView.getY());
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    public void checkforKeyboard(final View activityRootView){

        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > dpToPx(Verification.this, 200)) { // if more than 200 dp, it's probably a keyboard...
                    // ... do something here
                    scrolltoRow();
                }
            }
        });

    }
}
