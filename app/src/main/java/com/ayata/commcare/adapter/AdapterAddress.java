package com.ayata.commcare.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.commcare.model.ModelAddress;
import com.ayata.commcare.R;

public class AdapterAddress extends RecyclerView.Adapter<AdapterAddress.MyViewHolder> {
    private static int sSelected = -1;
//    private  SingleClickListener sClickListener;
    private static SingleClickListener listener;
    private List<ModelAddress> list;
    private int layout;

//    public AdapterSelectDesignation(List<ModelDesignation> list,SingleClickListener sClickListener) {
//        this.list = list;
//        this.sClickListener=sClickListener;
//    }
    public AdapterAddress(List<ModelAddress> list, int layout) {
        this.list = list;
        this.layout=layout;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(this.layout, parent, false);
        return new MyViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(@NonNull AdapterAddress.MyViewHolder holder, int position) {
//        holder.btnCircle.setChecked(viewType == mSelectedItem);
holder.btnText.setText(list.get(position).getName());
        Log.d("holder","i was clicked"+position);


        if (sSelected == position) {
            holder.btnCircle.setChecked(true);
//            holder.layout.setBackgroundColor(Color.parseColor("#567845"));
            holder.layout.setBackgroundResource(R.drawable.radio_outer_select);
        } else {
            holder.btnCircle.setChecked(false);
            holder.layout.setBackgroundResource(R.drawable.radio_outer_unselect);
//            gray_background_curve
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout layout;
        private TextView btnText;
        private RadioButton btnCircle;
//private SingleClickListener listener;
    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
        this.layout=itemView.findViewById(R.id.linearlayout_designation);
        this.btnText = (TextView) itemView.findViewById(R.id.radioButton);
        this.btnCircle=(RadioButton)itemView.findViewById(R.id.radioButton_circle);
//        this.listener=listener;
            itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
                Log.d("error","i was clicked");

        sSelected = getAdapterPosition();
        listener.onItemClickListener(getAdapterPosition());
    }

//    @Override
//    public void onClick(View view) {
//        Log.d("error","i was clicked");
//
//        sSelected = getAdapterPosition();
//        listener.onItemClickListener(getAdapterPosition(), view);
//    }

}
    public void selectedItem(int i) {
        Log.d("helo","selected item" +i);
        notifyDataSetChanged();
    }
    public void setOnItemClickListener(SingleClickListener clickListener) {
        listener = clickListener;
    }

    public interface SingleClickListener {
        void onItemClickListener(int position);
    }
}
