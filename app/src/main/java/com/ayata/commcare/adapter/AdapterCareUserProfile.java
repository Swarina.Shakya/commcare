package com.ayata.commcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.commcare.model.ModelCareUserProfile;
import com.ayata.commcare.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class AdapterCareUserProfile extends RecyclerView.Adapter<AdapterCareUserProfile.modelViewHolder> {

    Context context;
    List<ModelCareUserProfile> listitem;
    OnCategoryListener onCategoryListener;


    public AdapterCareUserProfile(Context context, List<ModelCareUserProfile> listitem, OnCategoryListener onCategoryListener) {
        this.context = context;
        this.listitem = listitem;
        this.onCategoryListener= onCategoryListener;
    }

    @NonNull
    @Override
    public modelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_user_profile,parent,false);
        return new modelViewHolder(view,onCategoryListener);
    }

    @Override
    public void onBindViewHolder(@NonNull modelViewHolder holder, int position) {

        //holder.image.setImageResource(listitem.get(position).getImage());
        Glide.with(context).load(listitem.get(position).getImage()).into(holder.image);
        holder.name.setText(listitem.get(position).getName());
        holder.desp.setText(listitem.get(position).getDesp());
    }

    @Override
    public int getItemCount() {
        return listitem.size();
    }

    public class modelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image;
        TextView name, desp;
        OnCategoryListener onCategoryListener;

        public modelViewHolder(@NonNull View itemView,OnCategoryListener onCategoryListener) {
            super(itemView);
            this.onCategoryListener=onCategoryListener;

            image=(ImageView) itemView.findViewById(R.id.user_profile_care_image);
            name= itemView.findViewById(R.id.user_profile_care_name);
            desp= itemView.findViewById(R.id.user_profile_care_info);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onCategoryListener.onCategoryClick(getAdapterPosition());
        }
    }

    public interface OnCategoryListener{
        void onCategoryClick(int position);
    }
}
