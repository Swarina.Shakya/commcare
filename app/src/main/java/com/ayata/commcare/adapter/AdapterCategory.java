package com.ayata.commcare.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.commcare.model.ModelCategoryBox;
import com.ayata.commcare.R;
import com.bumptech.glide.Glide;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.CategoryViewHolder> {
    Context context;
public CategoryClickListener listener;

    List<ModelCategoryBox> list=new ArrayList<>();

    public AdapterCategory(List<ModelCategoryBox> list, Context context,CategoryClickListener listener) {
        this.context=context;
        this.list = list;
        this.listener=listener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_box_single,parent,false);

//        float value=getScreenWidth();
//view.getLayoutParams().width= (int)value;
        Log.d("gggggggggggggggg","hello"+view.getLayoutParams().width);

        return new CategoryViewHolder(view,listener);
    }


    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.header.setText(list.get(position).getHeader());
        holder.paragraph.setText(list.get(position).getParagraph());
        Glide.with(context).load(list.get(position).getImage()).into(holder.image);
        //holder.image.setImageResource(list.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
private TextView header,paragraph;
private ImageView image;
public CategoryClickListener listener;

        public CategoryViewHolder(@NonNull View itemView,CategoryClickListener listener) {
            super(itemView);
            header=itemView.findViewById(R.id.text_header);
            image=itemView.findViewById(R.id.category_image);
            paragraph=itemView.findViewById(R.id.text_paragraph);
            this.listener=listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.SingleCategoryClick(getAdapterPosition());
        }
    }
    public interface CategoryClickListener{
        void SingleCategoryClick(int position);
    }
}
