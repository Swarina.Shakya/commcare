package com.ayata.commcare.adapter;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.commcare.model.ModelHomeBox;
import com.ayata.commcare.R;

public class AdapterHome extends RecyclerView.Adapter<AdapterHome.HomeViewHolder> {
    Context context;
    int[] images={R.drawable.file1,R.drawable.file2,R.drawable.contract};
    static GradientDrawable gradientDrawable,gradientDrawable2;
    static int[] color;
    List<ModelHomeBox> list=new ArrayList<>();
    public AdapterHome(List<ModelHomeBox> list,Context context) {
        this.context=context;
        color=context.getResources().getIntArray(R.array.rainbow);
        this.list = list;
    }
    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_box_single,parent,false);
        return new HomeViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        gradientDrawable.setColor(color[position]);
//        holder.imageview.setColorFilter(ContextCompat.getColor(context, color[position]), android.graphics.PorterDuff.Mode.SRC_IN);
//        holder.imageview.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
        holder.imageview.setColorFilter(color[position]); // White Tint
        holder.imageView2.setImageResource(images[position]);
//holder.relativeLayout.setBackgroundColor(color[1]);
        holder.header.setText(list.get(position).getHeader());
        holder.paragraph.setText(list.get(position).getParagraph());
        holder.number.setText(String.valueOf(list.get(position).getNumber()));
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
    public class HomeViewHolder extends RecyclerView.ViewHolder {
        private TextView header,number,paragraph;
        private ImageView imageview,imageView2;
        private RelativeLayout relativeLayout;
        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            imageview=itemView.findViewById(R.id.home_folder);
            imageView2=itemView.findViewById(R.id.home_folder_inside);
            header=itemView.findViewById(R.id.text_header);
            number=itemView.findViewById(R.id.text_no);
            paragraph=itemView.findViewById(R.id.text_paragraph);
            relativeLayout=itemView.findViewById(R.id.relative_single_box_home);
            gradientDrawable=(GradientDrawable) relativeLayout.getBackground();
        }
    }
}


