package com.ayata.commcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.commcare.model.ModelPatientFollowuP;
import com.ayata.commcare.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class AdapterPatientFollowUp extends RecyclerView.Adapter<AdapterPatientFollowUp.modelViewHolder> {

    Context context;
    List<ModelPatientFollowuP> listitem;
    private OnPatientFollowUpClickListener onPatientFollowUpClickListener;

    public AdapterPatientFollowUp(Context context, List<ModelPatientFollowuP> listitem, OnPatientFollowUpClickListener onPatientFollowUpClickListener) {
        this.context = context;
        this.listitem = listitem;
        this.onPatientFollowUpClickListener= onPatientFollowUpClickListener;
    }

    @NonNull
    @Override
    public modelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_patient_follow_up,parent,false);
        return new modelViewHolder(view, onPatientFollowUpClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull modelViewHolder holder, int position) {

        holder.lastdate.setText(listitem.get(position).getLastdate());
        holder.nextdate.setText(listitem.get(position).getNextdate());
        holder.title.setText(listitem.get(position).getCheckup_header());
        //holder.image.setImageResource(listitem.get(position).getImage());
        Glide.with(context).load(listitem.get(position).getImage()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return listitem.size();
    }

    public class modelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView lastdate,nextdate,title;
        ImageView image;

        OnPatientFollowUpClickListener onPatientFollowUpClickListener;

        public modelViewHolder(@NonNull View itemView, OnPatientFollowUpClickListener onPatientFollowUpClickListener) {
            super(itemView);
            this.onPatientFollowUpClickListener= onPatientFollowUpClickListener;

            lastdate= itemView.findViewById(R.id.pfu_last_date);
            nextdate=itemView.findViewById(R.id.pfu_next_date);
            title = itemView.findViewById(R.id.pfu_checkup_header);
            image = itemView.findViewById(R.id.pfu_patient_image);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onPatientFollowUpClickListener.onPatientFollowUpClick(getAdapterPosition());
        }
    }

    public interface OnPatientFollowUpClickListener{
        void onPatientFollowUpClick(int position);
    }


}
