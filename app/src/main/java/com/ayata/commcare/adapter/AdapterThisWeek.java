package com.ayata.commcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.commcare.model.ModelThisWeek;
import com.ayata.commcare.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class AdapterThisWeek extends RecyclerView.Adapter<AdapterThisWeek.modelViewHolder>{

    private Context context;
    private List<ModelThisWeek> listitem;
    private OnThisWeekClickListener onThisWeekClickListener;

    public AdapterThisWeek(Context context, List<ModelThisWeek> listitem, OnThisWeekClickListener onThisWeekClickListener) {
        this.context = context;
        this.listitem = listitem;
        this.onThisWeekClickListener= onThisWeekClickListener;
    }

    @NonNull
    @Override
    public modelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_video_articles_this_week,parent,false);

        return new modelViewHolder(view, onThisWeekClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull modelViewHolder holder, int position) {
        //holder.image.setImageResource(listitem.get(position).getImage());
        Glide.with(context).load(listitem.get(position).getImage()).into(holder.image);
        holder.header.setText(listitem.get(position).getTitle());
        holder.name.setText(listitem.get(position).getName());
        holder.time.setText(listitem.get(position).getTime());
    }

    @Override
    public int getItemCount() {
        return listitem.size();
    }

    public class modelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image;
        TextView header,name,time;
        OnThisWeekClickListener onThisWeekClickListener;

        public modelViewHolder(@NonNull View itemView, OnThisWeekClickListener onThisWeekClickListener) {
            super(itemView);

            this.onThisWeekClickListener= onThisWeekClickListener;
            image= itemView.findViewById(R.id.rvatw_image);
            header= itemView.findViewById(R.id.rvatw_header);
            name=itemView.findViewById(R.id.rvatw_desp1);
            time= itemView.findViewById(R.id.rvatw_desp2);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onThisWeekClickListener.onThisWeekClick(getAdapterPosition());
        }
    }

    public interface OnThisWeekClickListener{
        void onThisWeekClick(int position);
    }
}
