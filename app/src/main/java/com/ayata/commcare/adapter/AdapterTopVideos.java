package com.ayata.commcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ayata.commcare.model.ModelTopVideos;
import com.ayata.commcare.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class AdapterTopVideos extends RecyclerView.Adapter<AdapterTopVideos.modelViewHolder> {

    private Context context;
    private List<ModelTopVideos> listitem;
    private OnTopVideosClickListener onTopVideosClickListener;


    public AdapterTopVideos(Context context, List<ModelTopVideos> listitem, OnTopVideosClickListener onTopVideosClickListener) {
        this.context = context;
        this.listitem = listitem;
        this.onTopVideosClickListener= onTopVideosClickListener;
    }

    @NonNull
    @Override
    public modelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_video_articles_top_videos,parent,false);
        return new modelViewHolder(view, onTopVideosClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull modelViewHolder holder, int position) {

        //holder.image.setImageResource(listitem.get(position).getImage());
        Glide.with(context).load(listitem.get(position).getImage()).into(holder.image);
        holder.header.setText(listitem.get(position).getTitle());
        holder.desp.setText(listitem.get(position).getDesp());

    }

    @Override
    public int getItemCount() {
        return listitem.size();
    }

    public class modelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image;
        TextView header;
        TextView desp;
        OnTopVideosClickListener onTopVideosClickListener;

        public modelViewHolder(@NonNull View itemView, OnTopVideosClickListener onTopVideosClickListener) {
            super(itemView);
            this.onTopVideosClickListener= onTopVideosClickListener;

            image=itemView.findViewById(R.id.rvatv_image);
            header=itemView.findViewById(R.id.rvatv_header);
            desp=itemView.findViewById(R.id.rvatv_desp);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onTopVideosClickListener.onTopVideoClick(getAdapterPosition());
        }
    }

    public interface OnTopVideosClickListener{
        void onTopVideoClick(int position);
    }
}
