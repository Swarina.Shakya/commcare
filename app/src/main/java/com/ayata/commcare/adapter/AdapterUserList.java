package com.ayata.commcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ayata.commcare.model.ModelUserList;
import com.ayata.commcare.R;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterUserList extends RecyclerView.Adapter<AdapterUserList.UserListViewHolder> {
    Context context;


    List<ModelUserList> list=new ArrayList<>();

    OnUserClickListenser onUserClickListenser;

    public AdapterUserList(List<ModelUserList> list, Context context, OnUserClickListenser onUserClickListenser) {
        this.context=context;
        this.list = list;
        this.onUserClickListenser = onUserClickListenser;
    }

    @NonNull
    @Override
    public UserListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.userlist_people_single,parent,false);
        return new UserListViewHolder(view, onUserClickListenser);
    }

    @Override
    public void onBindViewHolder(@NonNull UserListViewHolder holder, int position) {


        Glide.with(context).load(list.get(position).getUser_image()).into(holder.imageview);
        //holder.imageview.setImageResource(list.get(position).getUser_image());
        holder.username.setText(list.get(position).getUser_name());
        holder.useraddress.setText(String.valueOf(list.get(position).getUser_address()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class UserListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView username,useraddress;
        private CircularImageView imageview;

        private OnUserClickListenser onUserClickListenser;

        public UserListViewHolder(@NonNull View itemView, OnUserClickListenser onUserClickListenser) {
            super(itemView);
            this.onUserClickListenser= onUserClickListenser;
            imageview=itemView.findViewById(R.id.userimg);
           username=itemView.findViewById(R.id.username);
           useraddress=itemView.findViewById(R.id.useraddress);

           itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onUserClickListenser.onUserClick(getAdapterPosition());
        }
    }

    public interface OnUserClickListenser{
        void onUserClick(int position);
    }
}
