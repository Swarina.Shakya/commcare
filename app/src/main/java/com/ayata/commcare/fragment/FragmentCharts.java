package com.ayata.commcare.fragment;


import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.ayata.commcare.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCharts extends Fragment {

    private LineChart lineChart;
    private Spinner spinner;

    public FragmentCharts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_charts, container, false);

        lineChart= view.findViewById(R.id.fc_line_chart);
        customiseLineChart();

        spinner= view.findViewById(R.id.fc_spinner);
        set_spinner();

        return view;
    }

    private void set_spinner(){
        String[] timeline={"Monthly","Weekly","Yearly"};

        spinner.setPrompt("Select Time");
        ArrayAdapter<String> adapter= new ArrayAdapter<>(getContext(),R.layout.dropdown,R.id.dropdown_text, timeline);
        spinner.setAdapter(adapter);
        spinner.setSelected(true);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                setLineChartData(5, (position+300));
                lineChart.notifyDataSetChanged();
                lineChart.invalidate();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void customiseLineChart(){
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);

        lineChart.setDrawGridBackground(false);

        lineChart.getDescription().setEnabled(false);

//        setLineChartData(5, 500);

        String[] values= new String[]{"Jan","Feb","Mar","Apr","May"};
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setValueFormatter(new MyXAxisValueFormatter(values));
        xAxis.setGranularity(1);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);

        YAxis left= lineChart.getAxisLeft();
        left.setDrawGridLines(false);
        left.setDrawLabels(false);
        left.setDrawAxisLine(false);

        lineChart.getAxisRight().setEnabled(false);

        Legend legend= lineChart.getLegend();
        legend.setYOffset(20f);
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        legend.setOrientation(Legend.LegendOrientation.VERTICAL);
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setYEntrySpace(5f);

        lineChart.setExtraTopOffset(10f);
    }

    private void setLineChartData(int count , int range){

        ArrayList<Entry> yval1= new ArrayList<>();
        for(int i = 0;i<count;i++ ){
            float val= (float) (Math.random()*range)+ 50;
            yval1.add(new Entry(i,val));
        }

        ArrayList<Entry> yval2= new ArrayList<>();
        for(int i = 0;i<count;i++ ){
            float val= (float) (Math.random()*range)+ 150;
            yval2.add(new Entry(i,val));
        }

        ArrayList<Entry> yval3= new ArrayList<>();
        for(int i = 0;i<count;i++ ){
            float val= (float) (Math.random()*range)+ 250;
            yval3.add(new Entry(i,val));
        }

        LineDataSet set1, set2, set3;

        set1= new LineDataSet(yval1,"Set1");
        set2= new LineDataSet(yval2,"Set2");
        set3= new LineDataSet(yval3, "Set3");

        LineData lineData= new LineData(set1,set2,set3);

        set1.setValueTextColor(getResources().getColor(R.color.chart_line1));
        set2.setValueTextColor(getResources().getColor(R.color.chart_line2));
        set3.setValueTextColor(getResources().getColor(R.color.chart_line3));
        set1.setColor(getResources().getColor(R.color.chart_line1));
        set2.setColor(getResources().getColor(R.color.chart_line2));
        set3.setColor(getResources().getColor(R.color.chart_line3));

        set1.setCircleColor(getResources().getColor(R.color.chart_line1));
        set1.setDrawCircleHole(false);
        set1.setCircleRadius(1.5f);

        set2.setCircleColor(getResources().getColor(R.color.chart_line2));
        set2.setDrawCircleHole(false);
        set2.setCircleRadius(1.5f);

        set3.setCircleColor(getResources().getColor(R.color.chart_line3));
        set3.setDrawCircleHole(false);
        set3.setCircleRadius(1.5f);

        lineChart.setData(lineData);
    }

    public class MyXAxisValueFormatter extends IndexAxisValueFormatter {

        private String[] values;

        public MyXAxisValueFormatter(String[] values) {
            this.values = values;
        }

        @Override
        public String getFormattedValue(float value) {
            return values[(int)value];
        }
    }

}
