package com.ayata.commcare.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ayata.commcare.model.ModelHomeBox;
import com.ayata.commcare.R;
import com.ayata.commcare.adapter.AdapterHome;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class FragmentHome extends Fragment {
    RecyclerView recyclerView;
    AdapterHome adapter;
    List<ModelHomeBox> list  =new ArrayList<>();

    private BarChart barChart;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view= inflater.inflate(R.layout.fragment_fragment_home, container, false);
       recyclerView=view.findViewById(R.id.recycle_container);
       initRecycler();

        barChart=(BarChart) view.findViewById(R.id.home_barchart);

        barChart.setMaxVisibleValueCount(40);
        setBarChartData(15);

        return  view;
    }

    public void initRecycler(){
        RecyclerView.LayoutManager manager=new LinearLayoutManager(getContext());
        ((LinearLayoutManager) manager).setOrientation(LinearLayoutManager.HORIZONTAL);
        adapter=new AdapterHome(list,getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        DataPrepare();
        adapter.notifyDataSetChanged();
    }

public void DataPrepare(){

        list.add(new ModelHomeBox(2600,"header1","paragraph1"));
    list.add(new ModelHomeBox(1600,"header2","paragraph2"));
    list.add(new ModelHomeBox(00,"header3","paragraph3"));
}

    public void setBarChartData(int count){
        ArrayList<BarEntry> yvalues= new ArrayList<>();

        for(int i=0; i<count; i++){
            float val1= (float)(Math.random()*count)+20;
            float val2= (float)(Math.random()*count)+5;
            float val3= (float)(Math.random()*count)+10;

            yvalues.add(new BarEntry(i,new float[]{val1,val2,val3}));
        }
        BarDataSet set1;
        set1= new BarDataSet(yvalues,"Statictics");
        set1.setDrawIcons(false);

        set1.setStackLabels(new String[]{"A","B","C"});

        BarData data= new BarData(set1);
        data.setValueFormatter(new MyValueFormatter());

        barChart.setData(data);
        barChart.setFitBars(true);
        barChart.invalidate();

        set1.setColors(getResources().getIntArray(R.array.bar_chart));

        data.setBarWidth(0.3f);
        barChart.getDescription().setEnabled(false);

        barChart.getAxisRight().setEnabled(false);
        barChart.getAxisLeft().setDrawLabels(false);
        barChart.getAxisLeft().setDrawAxisLine(false);
        barChart.getAxisLeft().setStartAtZero(true);


        XAxis xAxis= barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        //set false--to disable zoom in chart
        barChart.setScaleEnabled(false);

        Legend legend= barChart.getLegend();
        legend.setYOffset(15f);
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);

        barChart.setExtraTopOffset(10f);
        barChart.setExtraBottomOffset(15f);
    }

    public class MyAxisValueFormatter extends IndexAxisValueFormatter {

        private DecimalFormat decimalFormat;

        @Override
        public String getFormattedValue(float value) {
            return decimalFormat.format(value)+"$";

        }
    }

    public class MyValueFormatter extends ValueFormatter {

        private DecimalFormat decimalFormat;

        public MyValueFormatter() {
            decimalFormat= new DecimalFormat("######.0");
        }

        @Override
        public String getFormattedValue(float value) {
            //return decimalFormat.format(value)+"$";
            return "";
        }
    }

}
