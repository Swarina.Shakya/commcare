package com.ayata.commcare.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ayata.commcare.GetStarted;
import com.ayata.commcare.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProfile extends Fragment implements View.OnClickListener {


    public FragmentProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_profile, container, false);


        LinearLayout logout = view.findViewById(R.id.profile_logout);
        logout.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        Intent intent= new Intent(getContext(), GetStarted.class);
        startActivity(intent);
        LinearLayout logout = getView().findViewById(R.id.profile_logout);

}
}
