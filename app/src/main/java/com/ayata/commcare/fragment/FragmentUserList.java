package com.ayata.commcare.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ayata.commcare.MainUserProfile;
import com.ayata.commcare.model.ModelUserList;
import com.ayata.commcare.PatientFollowUp;
import com.ayata.commcare.R;
import com.ayata.commcare.adapter.AdapterUserList;

import java.util.ArrayList;
import java.util.List;

public class FragmentUserList extends Fragment implements AdapterUserList.OnUserClickListenser {

    RecyclerView recyclerView;
    AdapterUserList adapter;
    List<ModelUserList> list  =new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view= inflater.inflate(R.layout.fragment_user_list, container, false);

       TextView seeall= view.findViewById(R.id.ful_see_all);
       seeall.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent= new Intent(getContext(), PatientFollowUp.class);
               startActivity(intent);
           }
       });
       
        recyclerView=view.findViewById(R.id.recycle_container);
        RecyclerView.LayoutManager manager=new LinearLayoutManager(getContext());
        ((LinearLayoutManager) manager).setOrientation(LinearLayoutManager.VERTICAL);
        adapter=new AdapterUserList(list,getContext(), this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        DataPrepare();
       return view;
    }

    public void DataPrepare(){
        list.add(new ModelUserList("Ronesh Shrestha","New Baneshwor",R.drawable.person1));
        list.add(new ModelUserList("Swarina Shakya","Old Baneshwor",R.drawable.person2));
        list.add(new ModelUserList("Shristy Shakya","Teku",R.drawable.person3));

    }

    @Override
    public void onUserClick(int position) {
        Intent intent= new Intent(getContext(), MainUserProfile.class);
        startActivity(intent);
    }
}
