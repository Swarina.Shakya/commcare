package com.ayata.commcare.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ayata.commcare.model.ModelCareUserProfile;
import com.ayata.commcare.R;
import com.ayata.commcare.adapter.AdapterCareUserProfile;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentUserProfile extends Fragment implements AdapterCareUserProfile.OnCategoryListener{


    private RecyclerView recyclerView;
    private AdapterCareUserProfile adapterCareUserProfile;
    private LinearLayoutManager linearLayoutManager;
    private List<ModelCareUserProfile> listitem;


    public FragmentUserProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_user_profile, container, false);

        Button btn= view.findViewById(R.id.user_profile_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               //MainPage.fragmentManager.beginTransaction().replace(getId(),new FragmentCategory(),null).commit();
               // MainUserProfile.fragmentManager.beginTransaction().replace(getId(),new FragmentCategory(),null).commit();

                getActivity().getSupportFragmentManager().beginTransaction().replace(getId(),new FragmentCategory(),null).addToBackStack("userprofile").commit();
            }
        });

        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_user_profile);
        listitem= new ArrayList<>();
        adapterCareUserProfile= new AdapterCareUserProfile(getContext(),listitem,this);
        linearLayoutManager=new LinearLayoutManager(getContext());
        recyclerView.setAdapter(adapterCareUserProfile);
        recyclerView.setLayoutManager(linearLayoutManager);
        dataPrepare();
        return view;

    }

    public void dataPrepare(){

        listitem.add(new ModelCareUserProfile(R.drawable.ic_pregnancy,"Maternal Care","Last logged: 21 Mangsir 2076"));
        listitem.add(new ModelCareUserProfile(R.drawable.ic_circle_outline,"Non Communicable Disease","Last Logged: 21 Mangsir 2076"));
    }

    @Override
    public void onCategoryClick(int position) {

        Log.d("category", "onCategoryClick: clicked");
//        Intent intent= new Intent(getContext(), CategoryTraining.class);
//        startActivity(intent);
        //MainPage.fragmentManager.beginTransaction().replace(R.id.fragment_main_page,new FragmentCategory(),null).commit();
        //MainUserProfile.fragmentManager.beginTransaction().replace(getId(),new FragmentCategory(),null).commit();

        getActivity().getSupportFragmentManager().beginTransaction().replace(getId(),new FragmentCategory(),null).addToBackStack("userprofile").commit();


    }
}
