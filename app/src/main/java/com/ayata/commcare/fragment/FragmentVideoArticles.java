package com.ayata.commcare.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ayata.commcare.model.ModelThisWeek;
import com.ayata.commcare.model.ModelTopVideos;
import com.ayata.commcare.R;
import com.ayata.commcare.SinglePost;
import com.ayata.commcare.adapter.AdapterThisWeek;
import com.ayata.commcare.adapter.AdapterTopVideos;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentVideoArticles extends Fragment implements AdapterTopVideos.OnTopVideosClickListener, AdapterThisWeek.OnThisWeekClickListener{

    private RecyclerView recyclerView;
    private AdapterTopVideos adapterTopVideos;
    private List<ModelTopVideos> listitem1;
    private AdapterThisWeek adapterThisWeek;
    private List<ModelThisWeek>listitem2;
    private LinearLayoutManager layoutManager;


    public FragmentVideoArticles() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_video_articles, container, false);

        recyclerViewTopVideo(view);
        recyclerViewThisWeek(view);

       return view;
    }

    public void recyclerViewTopVideo(View view){
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_va_top_video);
        listitem1= new ArrayList<>();
        adapterTopVideos= new AdapterTopVideos(getContext(),listitem1, this);
        layoutManager= new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapterTopVideos);
        recyclerView.setLayoutManager(layoutManager);
        dataPrepareTopVideos();
    }

    public void recyclerViewThisWeek(View view){
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_va_this_week);
        listitem2= new ArrayList<>();
        adapterThisWeek= new AdapterThisWeek(getContext(),listitem2, this);
        layoutManager= new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapterThisWeek);
        recyclerView.setLayoutManager(layoutManager);
        dataPrepareThisWeek();
    }


    public void dataPrepareTopVideos(){
        ModelTopVideos data= new ModelTopVideos(R.drawable.image1, "How to treat a patient?","Know how to treat a patient.");
        listitem1.add(data);
        data= new ModelTopVideos(R.drawable.image1, "How to treat a patient?","Know how to treat a patient.");
        listitem1.add(data);
        data= new ModelTopVideos(R.drawable.image1, "How to treat a patient?","Know how to treat a patient.");
        listitem1.add(data);
        data= new ModelTopVideos(R.drawable.image1, "How to treat a patient?","Know how to treat a patient.");
        listitem1.add(data);
    }


    public void dataPrepareThisWeek(){

        listitem2.add(new ModelThisWeek(R.drawable.image2,"New medicines for Cholera","XYZ Medicals","55 mins ago"));
        listitem2.add(new ModelThisWeek(R.drawable.image2,"New medicines for Cholera","ABC Organization","20 mins ago"));
        listitem2.add(new ModelThisWeek(R.drawable.image2,"New medicines for Cholera","XYZ Medicals","55 mins ago"));
        listitem2.add(new ModelThisWeek(R.drawable.image2,"New medicines for Cholera","XYZ Medicals","55 mins ago"));
        listitem2.add(new ModelThisWeek(R.drawable.image2,"New medicines for Cholera","XYZ Medicals","55 mins ago"));
    }

    @Override
    public void onTopVideoClick(int position) {
        Intent intent= new Intent(getContext(), SinglePost.class);
        startActivity(intent);
    }

    @Override
    public void onThisWeekClick(int position) {
        Intent intent= new Intent(getContext(),SinglePost.class);
        startActivity(intent);
    }
}
