package com.ayata.commcare.model;

import android.graphics.drawable.Drawable;

public class ModelCareUserProfile {

    private int image;
    private String name,desp;

    public ModelCareUserProfile(int image, String name, String desp) {
        this.image = image;
        this.name = name;
        this.desp = desp;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }
}
