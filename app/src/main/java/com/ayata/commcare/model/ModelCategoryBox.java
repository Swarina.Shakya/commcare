package com.ayata.commcare.model;

public class ModelCategoryBox extends ModelParent {
    private int image;
    public ModelCategoryBox(String header,String paragraph,int image) {
    super(header,paragraph);
    this.image=image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
