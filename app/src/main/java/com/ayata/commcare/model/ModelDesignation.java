package com.ayata.commcare.model;

public class ModelDesignation {
    private int designation_id;
    private String designation_name;

    public ModelDesignation(String designation_name) {
        this.designation_name = designation_name;
    }

    public int getDesignation_id() {
        return designation_id;
    }

    public void setDesignation_id(int designation_id) {
        this.designation_id = designation_id;
    }

    public ModelDesignation() {
    }

    public String getDesignation_name() {
        return designation_name;
    }

    public void setDesignation_name(String designation_name) {
        this.designation_name = designation_name;
    }

}
