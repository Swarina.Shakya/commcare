package com.ayata.commcare.model;

public class ModelHomeBox extends ModelParent {
    private int number;


    public ModelHomeBox(int number, String header, String paragraph) {
        super(header,paragraph);
        this.number = number;
    }

    public ModelHomeBox(String header, String paragraph) {
        super(header,paragraph);
    }



    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }


}
