package com.ayata.commcare.model;

public class ModelParent {
    private long id;
    private String header;
    private String paragraph;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getParagraph() {
        return paragraph;
    }

    public ModelParent() {
    }

    public ModelParent(String header, String paragraph) {
        this.header = header;
        this.paragraph = paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }
}
