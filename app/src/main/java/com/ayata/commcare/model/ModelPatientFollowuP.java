package com.ayata.commcare.model;

public class ModelPatientFollowuP {

    private String lastdate, nextdate, checkup_header;
    private int image;

    public ModelPatientFollowuP(String lastdate, String nextdate, String checkup_header, int image) {
        this.lastdate = lastdate;
        this.nextdate = nextdate;
        this.checkup_header = checkup_header;
        this.image = image;
    }

    public String getLastdate() {
        return lastdate;
    }

    public void setLastdate(String lastdate) {
        this.lastdate = lastdate;
    }

    public String getNextdate() {
        return nextdate;
    }

    public void setNextdate(String nextdate) {
        this.nextdate = nextdate;
    }

    public String getCheckup_header() {
        return checkup_header;
    }

    public void setCheckup_header(String checkup_header) {
        this.checkup_header = checkup_header;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
