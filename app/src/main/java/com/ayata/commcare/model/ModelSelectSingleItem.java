package com.ayata.commcare.model;

public class ModelSelectSingleItem {
    private int id;
    private String name;

    public ModelSelectSingleItem(String name) {
        this.name = name;
    }

    public ModelSelectSingleItem() {
    }

    public ModelSelectSingleItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
