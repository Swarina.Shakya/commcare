package com.ayata.commcare.model;

public class ModelThisWeek {

    private int image;
    private String title,name,time;

    public ModelThisWeek(int image, String title, String name, String time) {
        this.image = image;
        this.title = title;
        this.name = name;
        this.time = time;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
