package com.ayata.commcare.model;

public class ModelUserList {
    private int user_id;
    private String user_name;
    private String user_address;

    private int user_image;

    public ModelUserList(String user_name, String user_address, int user_image) {

        this.user_name = user_name;
        this.user_address = user_address;
        this.user_image = user_image;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_address() {
        return user_address;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

    public int getUser_image() {
        return user_image;
    }

    public void setUser_image(int user_image) {
        this.user_image = user_image;
    }
}
